package com.example.customview.CustomeDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.customview.R;

public class LoginDialog extends DialogFragment {

    private LoginDialogListener loginDialogListener;

    public interface LoginDialogListener{
        void getLogin(String email, String password, boolean remember);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        loginDialogListener = (LoginDialogListener) context;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.login_dialog_layout, null);

        builder.setView(view);

        final Button btnCancel = view.findViewById(R.id.dialog_btn_cancel);
        final Button btnLogin = view.findViewById(R.id.dialog_btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.create();
    }
}
