package com.example.customview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button bthello = findViewById(R.id.main_bt_helloworld);
        final Button btstart = findViewById(R.id.main_bt_start);
        final ImageView imgTotoru = findViewById(R.id.main_img_totoru);


        bthello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animFadeIn = AnimationUtils.loadAnimation(
                        MainActivity.this, R.anim.fade_in_anime
                );
                bthello.startAnimation(animFadeIn);

            }
        });

        btstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animStart = AnimationUtils.loadAnimation(
                        MainActivity.this,R.anim.traslate_animation
                );

                imgTotoru.startAnimation(animStart);

                animStart.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
//                        ScaleAnimation animScale = new ScaleAnimation(1,0,1,0,180,180);
//                        animScale.setDuration(1000);
//                        imgTotoru.startAnimation(animScale);

                        Animation animRotate = AnimationUtils.loadAnimation(
                                MainActivity.this, R.anim.rotate_animation
                        );
                        imgTotoru.startAnimation(animRotate);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {


                    }
                });

            }
        });



    }
}
