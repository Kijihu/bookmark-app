package com.example.customview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HomeWork6Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work6);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId){

            case R.id.add_menu:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                final CharSequence[] users = {"Admin", "System", "Editor", "Poster", "QC"};

                builder.setTitle("Choose User:");

                builder.setItems(users, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LinearLayout layoutContainer = findViewById(R.id.layout_container);

                        CardView cardView = new CardView(HomeWork6Activity.this);
                        cardView.setCardElevation(4);
                        cardView.setRadius(15);
                        cardView.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_shape_yellow));
                        LinearLayout.LayoutParams cardLayout = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        );
                        cardLayout.setMargins(0, 5, 0 ,5);
                        cardView.setLayoutParams(cardLayout);

                        TextView textUserName = new TextView(HomeWork6Activity.this);
                        textUserName.setText(users[which]);
                        textUserName.setTextSize(18);
//                        textUserName.setTypeface(Typeface.DEFAULT_BOLD);

                        textUserName.setTextColor(getResources().getColor(R.color.color_white));

                        CardView.LayoutParams userNameLayout = new CardView.LayoutParams(
                                -1,-2
                        );
                        textUserName.setLayoutParams(userNameLayout);
                        cardView.addView(textUserName);

                        layoutContainer.addView(cardView);
                    }
                });
                builder.create().show();

                break;

            case R.id.remove_all_menu:

                LinearLayout linearLayout = findViewById(R.id.layout_container);

//                linearLayout.animate().alpha(0).setDuration(1000);

                linearLayout.removeAllViewsInLayout();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
