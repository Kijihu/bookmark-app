package com.example.customview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;

public class HomeWork5Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work5);

        final Button btShow = findViewById(R.id.home_btn_show);
        final Button btHind = findViewById(R.id.home_btn_hide);
        final LinearLayout lnMenu = findViewById(R.id.home_linear_menu);

        lnMenu.animate().translationY(-2000);
        btHind.setVisibility(View.GONE);

        btShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnMenu.animate().translationYBy(2000).setDuration(2000);
                btShow.setVisibility(View.GONE);
                btHind.setVisibility(View.VISIBLE);
        }
        });

        btHind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnMenu.animate().translationY(-2000).setDuration(2000);
                btHind.setVisibility(View.GONE);
                btShow.setVisibility(View.VISIBLE);
            }
        });

    }
}
