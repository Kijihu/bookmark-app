package com.example.customview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.customview.CustomeDialog.LoginDialog;

public class LoginDialogActivity extends AppCompatActivity {

    final static String LOGIN_DIALOG = "Login Dialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_dialog);

        final Button btnLogin = findViewById(R.id.login_btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginDialog loginDialog = new LoginDialog();
                loginDialog.setCancelable(false);
                loginDialog.show(getSupportFragmentManager(), LOGIN_DIALOG);
            }
        });

    }
}
