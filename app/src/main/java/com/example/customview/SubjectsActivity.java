package com.example.customview;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

public class SubjectsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);

        final Button btchoose = findViewById(R.id.subjects_bt_choose);

        btchoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(SubjectsActivity.this);

                final CharSequence[] subjects = {"Android", "IOS", "Design", "Java"};

                builder.setTitle("Subject");
                builder.setIcon(R.drawable.ic_001_facebook);

                //set body
                builder.setSingleChoiceItems(subjects, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(SubjectsActivity.this, subjects[which].toString(), Toast.LENGTH_SHORT).show();

                    }
                });

                builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int selectedIndex = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                        Object item = ((AlertDialog)dialog).getListView().getItemAtPosition(selectedIndex);
                        Toast.makeText(SubjectsActivity.this,item+"",Toast.LENGTH_SHORT).show();
                    }
                });

                builder.create().show();

            }
        });

    }

}
